﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Tip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string TipText;


    public void OnPointerEnter(PointerEventData eventData)
    {
        StatusBar.Instance.ShowTip(TipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StatusBar.Instance.HideTip();
    }
}
