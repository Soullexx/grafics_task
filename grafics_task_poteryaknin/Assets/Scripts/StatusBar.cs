﻿using UnityEngine;
using UnityEngine.UI;

public class StatusBar : MonoBehaviour
{
    public Text StatusText;

    public static StatusBar Instance { get; private set; }

    private string _persistentText;
    private Color _persistentColor;

    public void Start ()
    {
        Instance = this;
    }

    public void ShowError(string message)
    {
        StatusText.color = Color.red;
        StatusText.text = message;
    }

    public void ShowSuccess(string message)
    {
        StatusText.color = Color.green;
        StatusText.text = message;
    }

    public void ShowTip(string message)
    {
        _persistentText = StatusText.text;
        _persistentColor = StatusText.color;
        StatusText.text = message;
        StatusText.color = Color.white;
    }

    public void HideTip()
    {
        StatusText.text = _persistentText;
        StatusText.color = _persistentColor;
    }

}
