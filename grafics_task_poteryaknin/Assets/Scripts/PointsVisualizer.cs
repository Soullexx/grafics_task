﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsVisualizer : MonoBehaviour
{
    public GameObject PointPrefab;
    public Transform PointsParent;
    public float Scale = 1f;

    private GameObject[] _allSourcePoints;
    private GameObject[] _subselectionPoints;
    private GameObject _pivotGo;



    public void Start()
    {
        if (PointsParent == null)
        {
            PointsParent = transform;
        }
    }

    public GameObject[] VisualizePoints(Vector2[] points, Color color)
    {
        var pointsCount = points.Length;
        var pointsGos = new GameObject[pointsCount];
        for (var i = 0; i < pointsCount; i++)
        {
            var point = points[i];
            var p = Instantiate(PointPrefab, PointsParent);
            pointsGos[i] = p;
            var r = p.GetComponent<RectTransform>();
            r.offsetMax += point * Scale;
            r.offsetMin += point * Scale;
            r.GetComponent<Image>().color = color; 
        }

        return pointsGos;
    }

    public void VisualizeSourcePoints(Vector2[] points, Color color)
    {
        _allSourcePoints = VisualizePoints(points, color);
    }

    public void ClearSourcePoints()
    {
        DestroyGos(ref _allSourcePoints);
    }

    public void VisualizeSubselectionPoints(Vector2[] points)
    {
        ClearSubselectionPoints();
        _subselectionPoints = VisualizePoints(points, Color.green);
    }

    public void ClearSubselectionPoints()
    {
        DestroyGos(ref _subselectionPoints);
    }

    public void VisualizePivot(Vector2 pivot)
    {
        DestroyPivot();
        _pivotGo = VisualizePoints(new []{ pivot }, Color.black)[0];
    }

    public void DestroyPivot()
    {
        if (_pivotGo == null) return;
        Destroy(_pivotGo);
        _pivotGo = null;
    }

    private void DestroyGos(ref GameObject[] gameObjects)
    {
        if (gameObjects == null) return;

        for (var i = gameObjects.Length - 1; i > -1; i--)
        {
            Destroy(gameObjects[i]);
        }

        gameObjects = null;
    }
}
