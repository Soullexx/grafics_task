﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountourRotationTest : MonoBehaviour
{
    public PointsVisualizer Pv;
    public MarksController Mc;
    public bool PointRotation = true;
    public bool ContourRotation = true;
    public bool UseMassCenter = true;
    public int StartingMarkIndex = 0;
    // 0 means all
    public int MarksCount = 0;
    public Vector2 Point;
    public Vector2 Center;
    public float RotationAngle = 15;

	public void Start ()
    {
        Pv.VisualizePoints(new[] { Vector2.zero, Point, Center }, new Color32(0xD1, 0xFF, 0x00, 0xFF));
	    if (PointRotation) TestPointRotation();
        if (ContourRotation) TestContourRotation();
    }

    public void TestPointRotation()
    {
        var color = new Color32(0xD1, 0xFF, 0x00, 0xFF);
        var rotatedPoint = MarksRotator.RotatePointAround(Point, Center, RotationAngle);
        Pv.VisualizePoints(new[] { rotatedPoint }, color);

        rotatedPoint = MarksRotator.RotatePointAround(Point, Center, RotationAngle * 2f);
        Pv.VisualizePoints(new[] { rotatedPoint }, color);

        rotatedPoint = MarksRotator.RotatePointAround(Point, Center, RotationAngle * 3f);
        Pv.VisualizePoints(new[] { rotatedPoint }, color);

        rotatedPoint = MarksRotator.RotatePointAround(Point, Center, RotationAngle * 4f);
        Pv.VisualizePoints(new[] { rotatedPoint }, color);

        rotatedPoint = MarksRotator.RotatePointAround(Point, Center, RotationAngle * 5f);
        Pv.VisualizePoints(new[] { rotatedPoint }, color);
    }

    public void TestContourRotation()
    {
        var marks = Mc.GetMarks();
        if (MarksCount != 0)
        {
            marks = new List<Vector2>(marks).GetRange(StartingMarkIndex, MarksCount).ToArray();
        }
        Pv.VisualizePoints(marks, Color.green);
        if (UseMassCenter)
        {
            Center = MassCenterCalculator.GetMassCenter(marks);
        }
        marks = MarksRotator.RotatePoints(marks, Center, RotationAngle);
        Pv.VisualizePoints(marks, Color.red);
    }
}
