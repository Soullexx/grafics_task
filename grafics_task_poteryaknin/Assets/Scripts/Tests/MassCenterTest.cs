﻿using UnityEngine;

public class MassCenterTest : MonoBehaviour
{
    public void Start()
    {
        var pts1 = new[]
        {
            new Vector2(-1f, 0f),
            new Vector2(0f, 1f),
            new Vector2(1f, 0f),
            new Vector2(0f, -1f),
        };
        AssertMassCenter(pts1, Vector2.zero);
        var pts2 = new[]
        {
            Vector2.zero,
            Vector2.zero,
            Vector2.zero
        };
        AssertMassCenter(pts2, Vector2.zero);
        var pts3 = new[]
        {
            new Vector2(1.2345f, 2.3456f),
            new Vector2(4.123f, 3.123f),
            new Vector2(5.987f, 0.256f)
        };
        AssertMassCenter(pts3, new Vector2(3.7815f, 1.9082f));
    }

    private void AssertMassCenter(Vector2[] points, Vector2 correctAnswer)
    {
        var mc = MassCenterCalculator.GetMassCenter(points);
        var delta = correctAnswer - mc;

        if (delta.magnitude < 0.0001f)
        {
            Debug.Log("Ok");
        }
        else
        {
            Debug.LogError("MassCenterCalculator.GetMassCenter(...) given incorrect result.");
        }
    }
}
