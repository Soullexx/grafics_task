﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PointsVisualizerTest : MonoBehaviour
{
    public PointsVisualizer Pv;
    public MarksController Mc;

    public int StartIndex;
    public int Count;

    public void Start()
    {
        var marks = Mc.GetMarks();

        var fMarks = new List<Vector2>(marks).GetRange(StartIndex, Count).ToArray();
        Pv.VisualizePoints(fMarks, new Color32(0xD1, 0xFF,0x00, 0xFF));
    }
}
