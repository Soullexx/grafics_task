﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContourScaleTest : MonoBehaviour
{
    public PointsVisualizer Pv;
    public MarksController Mc;

    public int SubsetFirstIndx = 0;
    public int SubsetLength = 0; // 0 means all

    public float ScaleFactor = 1.5f;
    public Vector2 ScalingPivot = Vector2.zero;
    public bool UseMassCenter = true;

    private Vector2[] _marks;


    public void Start ()
    {
        _marks = Mc.GetMarks();
        if (SubsetLength != 0)
        {
            _marks = new List<Vector2>(_marks).GetRange(SubsetFirstIndx, SubsetLength).ToArray();
        }
        Pv.VisualizePoints(_marks, Color.green);
        if (UseMassCenter)
        {
            ScalingPivot = MassCenterCalculator.GetMassCenter(_marks);
        }

        Pv.VisualizePoints(new[] { Vector2.zero, ScalingPivot }, Color.black);

        for (var i = 0; i < 10; i++)
        {
            TestContourScale();
            ScaleFactor += 0.1f;
        }
    }

    public void TestContourScale()
    {
        _marks = MarksScaler.ScaleContourAbout(_marks, ScalingPivot, ScaleFactor);
        Pv.VisualizePoints(_marks, Color.red);
    }
}
