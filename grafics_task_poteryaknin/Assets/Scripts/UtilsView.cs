﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UtilsView : MonoBehaviour
{
    public InputField MarksInputField;

    public Button CalcShowMassCenterButton;

    public Toggle SubselectionToggle;
    public InputField SubselectionStartingIndexInputField;
    public InputField SubselectionLengthInputField;

    public Transform PivotControlsTr;
    public Toggle UseMassCenterToggle;
    public Toggle UseFaceCenterToggle;
    public InputField XInputField;
    public InputField YInputField;

    public Transform RotationControlsTr;
    public InputField RotationAngleInputField;

    public Transform ScaleControlsTr;
    public InputField ScaleFactorInputField;

    public PointsVisualizer PointsViz;

    public Text StatusText;

    private string _fileName = "landmarks";
    private Vector2[] _marks;
    private Vector2[] _marksSubselection;
    private Vector2 _subselectionMassCenter;
    private Vector2 _pivot;
    private Vector2 _allMarksMassCenter;

    public void LoadMarks()
    {
        _fileName = MarksInputField.text;
        var marksString = MarksLoader.LoadMarks(_fileName);
        if (string.IsNullOrEmpty(marksString))
        {
            StatusBar.Instance.ShowError("Не удалось загрузить метки из файла: " + _fileName);
            return;
        }

        var marks = MarksParser.ParseLandmarksString(marksString, true);
        if (marks == null)
        {
            StatusBar.Instance.ShowError("Не удалось прочитать метки из файла: " + _fileName);
            return;
        }
        PointsViz.ClearSourcePoints();
        PointsViz.ClearSubselectionPoints();
        PointsViz.VisualizeSourcePoints(marks, Color.blue);
        _marks = marks;
        _marksSubselection = marks;
        SubselectionToggle.interactable = true;
        _allMarksMassCenter = MassCenterCalculator.GetMassCenter(_marks);
        PointsViz.VisualizePivot(_allMarksMassCenter);
        SetInteractionInChildren(RotationControlsTr, true);
        SetInteractionInChildren(ScaleControlsTr, true);
        SetInteractionInChildren(PivotControlsTr, true);
        if (SubselectionToggle.isOn)
        {
            RegetSubselection();
        }
        else
        {
            UseFaceCenterToggle.isOn = true;
            SetPivotToAllMarks();
        }
        StatusBar.Instance.ShowSuccess("Метки загружены и прочитаны. Чёрным цветом показан центр масс всех точек");
    }

    public void CalculateShowMassCenter()
    {
        if (_marksSubselection == null)
        {
            StatusBar.Instance.ShowError("Нельзя посчитать центр масс. Загрузите сначала метки.");
            return;
        }

        _subselectionMassCenter = MassCenterCalculator.GetMassCenter(_marksSubselection);
        PointsViz.VisualizePivot(_subselectionMassCenter);
        UseMassCenterToggle.isOn = true;
        SetPivotToSubselection();
        StatusBar.Instance.ShowSuccess("Центр масс посчитан и изображён чёрной точкой.");
    }

    public void SwitchSubselectionMode()
    {
        if (SubselectionToggle.isOn)
        {
            SubselectionStartingIndexInputField.interactable = true;
            SubselectionLengthInputField.interactable = true;
            CalcShowMassCenterButton.interactable = true;
            RegetSubselection();
        }
        else
        {
            SubselectionStartingIndexInputField.interactable = false;
            SubselectionLengthInputField.interactable = false;
            CalcShowMassCenterButton.interactable = false;
            PointsViz.ClearSubselectionPoints();
            _marksSubselection = _marks;
        }
    }

    public void RegetSubselection()
    {
        int startingIndex;
        if (!int.TryParse(SubselectionStartingIndexInputField.text, out startingIndex))
        {
            StatusBar.Instance.ShowError("Взятие выборки отменено. Проверьте поле ввода \"Индекс первой отметки\".");
            return;
        }

        int subselectionLength;
        if (!int.TryParse(SubselectionLengthInputField.text, out subselectionLength))
        {
            StatusBar.Instance.ShowError("Взятие выборки отменено. Проверьте поле ввода \"Количество элементов\".");
            return;
        }

        if (startingIndex + subselectionLength > _marks.Length)
        {
            StatusBar.Instance.ShowError("Взятие выборки отменено. Выборка выходит за пределы множества исходных меток.");
            return;
        }

        _marksSubselection = new List<Vector2>(_marks).GetRange(startingIndex, subselectionLength).ToArray();
        PointsViz.VisualizeSubselectionPoints(_marksSubselection);

        if (UseMassCenterToggle.isOn)
        {
            SetPivotToSubselection();
        }

        StatusBar.Instance.ShowSuccess("Выборка взята успешно и отображена зелёным цветом.");
    }

    public void SetPivotToAllMarks()
    {
        SetPivot(_allMarksMassCenter);
    }

    public void SetPivotToSubselection()
    {
        _subselectionMassCenter = MassCenterCalculator.GetMassCenter(_marksSubselection);
        SetPivot(_subselectionMassCenter);
    }

    public void SetCustomPivot()
    {
        float x;
        if (!float.TryParse(XInputField.text, out x))
        {
            StatusBar.Instance.ShowError("Выбор опорной точки отменён. Проверьте поле ввода \"X\".");
            return;
        }
        float y;
        if (!float.TryParse(YInputField.text, out y))
        {
            StatusBar.Instance.ShowError("Выбор опорной точки отменён. Проверьте поле ввода \"Y\".");
            return;
        }
        SetPivot(new Vector2(x, y));
        StatusBar.Instance.ShowSuccess("Новая опорная точка выбрана.");
    }

    public void PickMassCenter()
    {
        StatusBar.Instance.ShowError("Данная функция доступна только в PRO версии");
    }

    public void Rotate()
    {
        float angle;
        if (!float.TryParse(RotationAngleInputField.text, out angle))
        {
            StatusBar.Instance.ShowError("Поворот отменён. Проверьте поле ввода \"Угол поворота\".");
            return;
        }

        _marksSubselection = MarksRotator.RotatePoints(_marksSubselection, _pivot, angle);
        PointsViz.VisualizeSubselectionPoints(_marksSubselection);
        StatusBar.Instance.ShowSuccess("Поворот осуществлён.");
    }

    public void Scale()
    {
        float scaleFactor;
        if (!float.TryParse(ScaleFactorInputField.text, out scaleFactor))
        {
            StatusBar.Instance.ShowError("Масштабирование отменено. Проверьте поле ввода \"К-т масшт-ия\".");
            return;
        }

        _marksSubselection = MarksScaler.ScaleContourAbout(_marksSubselection, _pivot, scaleFactor);
        PointsViz.VisualizeSubselectionPoints(_marksSubselection);
        StatusBar.Instance.ShowSuccess("Масштабирование осуществлено.");
    }

    public void Start()
    {
        MarksInputField.text = _fileName;
        SubselectionToggle.isOn = false;
        SubselectionToggle.interactable = false;
        SubselectionStartingIndexInputField.text = "42";
        SubselectionStartingIndexInputField.interactable = false;
        SubselectionLengthInputField.text = "6";
        SubselectionLengthInputField.interactable = false;
        CalcShowMassCenterButton.interactable = false;
        SetInteractionInChildren(RotationControlsTr, false);
        SetInteractionInChildren(ScaleControlsTr, false);
        SetInteractionInChildren(PivotControlsTr, false);
        RotationAngleInputField.text = "15";
        ScaleFactorInputField.text = "1.25";
    }

    private void SetInteractionInChildren(Transform parent, bool value)
    {
        var selectables = parent.GetComponentsInChildren<Selectable>();
        foreach (var selectable in selectables)
        {
            selectable.interactable = value;
        }
    }

    private void SetPivot(Vector2 newValue)
    {
        _pivot = newValue;
        XInputField.text = _pivot.x.ToString("N3");
        YInputField.text = _pivot.y.ToString("N3");
        PointsViz.VisualizePivot(_pivot);
    }
}
