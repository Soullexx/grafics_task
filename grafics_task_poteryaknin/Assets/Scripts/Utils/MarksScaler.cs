﻿using UnityEngine;

public class MarksScaler
{
    public static Vector2 ScalePointAbout(Vector2 point, Vector2 pivot, float scaleFactor)
    {
        var d = point - pivot;
        var res = scaleFactor * d + pivot;
        return res;
    }

    public static Vector2[] ScaleContourAbout(Vector2[] points, Vector2 pivot, float scaleFactor)
    {
        var pointsCount = points.Length;
        var res = new Vector2[pointsCount];
        for (var i = 0; i < pointsCount; i++)
        {
            res[i] = ScalePointAbout(points[i], pivot, scaleFactor);
        }
        return res;
    }
}
