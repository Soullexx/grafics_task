﻿using System.Linq;
using UnityEngine;

public class MassCenterCalculator
{
    public static Vector2 GetMassCenter(Vector2[] points)
    {
        return points.Aggregate(Vector2.zero, (current, point) => current + point) / points.Length;
    }
}
