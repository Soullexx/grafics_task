﻿using System.Collections.Generic;
using UnityEngine;

public class MarksParser
{
    public static Vector2[] ParseLandmarksString(string landmarksString, bool invertY = false)
    {
        if (landmarksString == null)
        {
            Debug.LogError("Landmarks.csv is broken");
            return null;
        }
        var marksStrings = landmarksString.Split(';');
        if (marksStrings.Length % 2 == 1)
        {
            Debug.LogError("Landmarks.csv is broken");
            return null;
        }
        // if .csv file is broken and consists of odd value, last value will be skipped 
        var marksCount = marksStrings.Length / 2;
        var res = new List<Vector2>(marksCount);
        for (var i = 0; i < marksCount; i++)
        {
            float x, y;
            if (!float.TryParse(marksStrings[2 * i], out x))
            {
                Debug.LogError("Can't parse float. Please check file.");
                continue;
            }
            if (!float.TryParse(marksStrings[2 * i + 1], out y))
            {
                Debug.LogError("Can't parse float. Please check file.");
                continue;
            }

            if (invertY) { y = -y; }
            res.Add(new Vector2(x, y));
        }
        return res.ToArray();
    }
}
