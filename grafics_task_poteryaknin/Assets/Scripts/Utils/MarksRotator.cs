﻿using UnityEngine;

public class MarksRotator
{
    public static Vector2 RotatePointAround(Vector2 point, Vector2 center, float angle)
    {
        return RotatePointAround(point, center, GetRotation2D(angle));
    }

    public static Vector2[] RotatePoints(Vector2[] points, Vector2 center, float angle)
    {
        var pointsCount = points.Length;
        var res = new Vector2[pointsCount];
        var r = GetRotation2D(angle);
        for (var i = 0; i < pointsCount; i++)
        {
            res[i] = RotatePointAround(points[i], center, r);
        }
        return res;
    }

    private static Vector2 RotatePointAround(Vector2 point, Vector2 center, Quaternion rotation)
    {
        var d = point - center;
        var res = (Vector2)(rotation * d) + center;
        return res;
    }

    private static Quaternion GetRotation2D(float angle)
    {
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
