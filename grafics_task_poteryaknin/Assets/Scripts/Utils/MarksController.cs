﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarksController : MonoBehaviour
{
    public string LandmarksFileName = "landmarks";

    void Start ()
    {
        var marks = GetMarks();
        if (marks == null)
        {
            Debug.LogError("Landmarks file parsing error. Cancelled.");
            return;
        }
        //Debug.Log(marks.Length);
    }

    public Vector2[] GetMarks()
    {
        var marksStr = MarksLoader.LoadMarks(LandmarksFileName);
        var marks = MarksParser.ParseLandmarksString(marksStr);
        return marks;
    }
	
	void Update ()
	{
		
	}
}
