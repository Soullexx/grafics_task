﻿using UnityEngine;

public class MarksLoader
{
    public static string LoadMarks(string fileName)
    {
        var marksString = Resources.Load<TextAsset>(fileName);
        if (marksString == null)
        {
            Debug.LogError("Can't load landmarks.csv");
            return null;
        }

        return marksString.text;
    }
}
